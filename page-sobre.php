<?php 
    // Template Name: Sobre
?>

<?php get_header(); ?>

<?php include get_template_directory().'/include/sobre.php'; ?>

<div class="rede_h1">
    <h1>Nos siga nas nossas rede sociais</h1>
</div>

<div class="redes">
    <?php if(get_field('instagram') != ''):?>

        <a href="<?php the_field('instagram')?>"><img src = "<?php echo get_stylesheet_directory_uri(  ); ?>/img/i.png" alt=""></a>

    <?php endif ?>

    <?php if(get_field('facebook') != ''):?>
    
        <a href="<?php the_field('facebook')?>"><img src = "<?php echo get_stylesheet_directory_uri(  ); ?>/img/f.png" alt=""></a>

    <?php endif ?>

    <?php if(get_field('twitter') != ''):?>
    
    <a href="<?php the_field('twitter')?>"><img src = "<?php echo get_stylesheet_directory_uri(  ); ?>/img/t.png" alt=""></a>

<?php endif ?>

</div>


<?php get_footer(); ?>