<?php 

$news = new WP_Query(
    array(
        'posts_per_page' => 3,
        'post_type' => 'noticia',
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'post_date',
        'order' => 'DESC',
    
        )
    );
?>

<?php if($news -> have_posts(  )):
    while($news -> have_posts(  )):
        $news -> the_post(  ); ?>
        <div>
            <h2><?php the_title(  ); ?></h2>
            <?php the_content( ); ?>
        </div>
    <?php endwhile;
else: ?>
    <p>Não temos notícias =(</p>
<?php endif; ?>

