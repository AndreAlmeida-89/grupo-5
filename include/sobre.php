<section class="sct-sobre">
    <div class="texto-sobre">
        <h1>Quem somos<br> &<br> O que fazemos</h1>
        <p>Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante. Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla facilisi. Ut fringilla. Suspendisse potenti. Nunc feugiat mi a tellus consequat imperdiet. Vestibulum sapien. </p>

        <?php if (!is_page('sobre')):?>
        <div id="saiba-mais">
            <a href="<?php echo get_home_url(  ) . '/sobre' ?>">Saiba mais</a>
        </div>
        <?php endif ?>

    </div>

    <div class="figura-sobre">
        <figure>
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/img/quem-somos.jpg' ?>" alt="Quem somos">
        </figure>
    </div>
</section>