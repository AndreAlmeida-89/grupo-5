<?php
$news = new WP_Query(
    array(
        'posts_per_page' => 4,
        'post_type' => 'noticia',
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'rand',
        'order' => 'DESC',
    
        )
    );
$num = 1;
$linkatual = get_permalink();
?>

<?php if($news -> have_posts(  )):
    while(($news -> have_posts(  )) && ($num < 4)):
        $news -> the_post(  );
        if(get_permalink() != $linkatual):$num += 1;
        ?>
            <li>
                <a href="<?php echo get_permalink() ?>"> <?php echo get_the_title() ?> </a>
            </li>
        <?php endif; endwhile;
else: ?>
    <p>Não temos notícias =(</p>
<?php endif; ?>