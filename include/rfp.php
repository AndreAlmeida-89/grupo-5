<?php
$produtos = new WP_Query(
    array(
        'posts_per_page' => 4,
        'post_type' => 'post',
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'rand',
        'order' => 'DESC',
    
        )
    );
?>

<?php if($produtos -> have_posts(  )):
    while($produtos -> have_posts(  )):$num += 1;
        $produtos -> the_post(  );
        ?>
            <div class="teste">
                <?php echo get_the_content() ?>
                <div class="prod"><a href="<?php echo get_permalink() ?>"></a></div>
           </div>
        <?php endwhile;
else: ?>
    <p>Não temos produtos =(</p>
<?php endif; ?>