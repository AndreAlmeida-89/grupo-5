<?php 
    // Template Name: Home Page
?>

<?php get_header(); ?>

<section class="sct-chamativa">
    <figure><img src="<?php echo get_stylesheet_directory_uri(  ); ?>/img/bg-chamativo.png" alt=""></figure>
    <div class="txt-sct-chamativa">
        <h1>Loja dedicada ao Naruto</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
    </div>
</section>

<?php include get_template_directory().'/include/sobre.php'; ?>

<section class="sct-produtos">
   
<div class="fotosonly"><?php include get_template_directory(  ).'/include/rfp.php'; ?></div>

<div class="to_noticias" id="saiba-mais"><a href="<?php echo get_home_url() ?>/produto">Ir para produtos</a></div>

</section>

<section class="sct-noticias">
<h1>Notícias</h1>
    <div classe="noticias">
        <?php include get_template_directory(  ).'/include/noticias.php'; ?>
    </div>

  <div class="to_noticias" id="saiba-mais"><a href="<?php echo get_home_url() ?>/noticias">Ir para noticias</a></div>

</section>

<div class="footer-division division"></div>


<?php get_footer(); ?>