<?php 
//Template Post Type: produtos
?>

<?php get_header(); ?>

<h1>Produtos</h1>

<main id="single-post" class="container">
    <div class="card">
        <div>
            <h1><?php the_title(); ?></h1>
        </div><?php
        while ( have_posts(  )):
            the_post(  );
            the_content( );
        endwhile; ?>
    </div>
</main>

<?php include get_template_directory(  ).'/include/randomproduto.php'; ?>

<?php get_footer( ); ?>