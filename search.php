<?php
get_header(); ?>
<div class="content-area">	
	<?php
	if ( have_posts() ) :
		if ( is_home() && ! is_front_page() ) :
            ?>
            <?php 	the_author(); ?>
			<?php $author = get_the_author(); ?>
			<header>
				<h1 class="page-title"><?php single_post_title(); ?></h1>
			</header>
			<?php
		endif;
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', 'archive' );
            ?>
            <a href="<?php the_permalink() ?>"> <h3> <?php the_title() ?> </h3> </a>
            <?php
		endwhile;
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif;
	?>
</div>
<?php
get_footer();