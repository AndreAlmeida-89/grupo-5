<?php 
    // Template Name: Noticias
?>

<?php get_header();



$news = new WP_Query(
    array(
        'posts_per_page' => 2,
        'paged' => $paged,
        'post_type' => "noticia",
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'post_date',
        'order' => 'DESC',
    
        )
    );
?>

<h1>Notícias</h1>

<?php get_search_form(  ) ?>

<?php if($news -> have_posts(  )):
    while($news -> have_posts(  )):
        $news -> the_post(  ); ?>
        <divc class="card border-primary mb-3">
            <h2><?php the_title(  ); ?></h2>
            <a href="<?php the_permalink() ?>">Link para Notícia!</a>
            <?php the_content( ); ?>
        </divc>
    <?php endwhile;
else: ?>
    <p>Não temos notícias =(</p>
<?php endif; ?>

<?php
$big = 999999999;
echo paginate_links( array(
    'base' => str_replace($big, '%#%', get_pagenum_link( $big )),
    'format' => '?paged=%#%',
    'current' => max(1, get_query_var('paged')),
    'prev_text' => __('Anterior'),
    'next_text' => __('Próximo'),
    'total' => $news->max_num_pages)
);
?>
<?php wp_reset_postdata(  ); ?>

<?php get_footer(); ?>