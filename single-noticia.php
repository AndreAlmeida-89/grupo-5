<?php 
//Template Post Type: noticia
?>

<?php get_header(); ?>
<main id="single-noticia" class="container">
    <div class="card">
        <div>
            <h1><?php the_title(); ?></h1>
            <div>
                <p>Data da Postagem: <?php echo get_the_date( ) ?></p>
                <p>Data de Edição: <?php echo get_the_modified_date( )?></p>
            </div>
        </div><?php
        while ( have_posts(  )):
            the_post(  );
            the_content( );
        endwhile; ?>
    </div>
</main>

<?php include get_template_directory(  ).'/include/randomnoticia.php'; ?>

<?php get_footer( ); ?>