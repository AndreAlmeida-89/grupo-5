<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <title>Loja Naruto</title>
</head>
<body>
    <header class="header">
      <a href="<?php if(is_front_page()){echo '#';}else{echo get_home_url();} ?>">
        <img src="<?php echo get_stylesheet_directory_uri(  ); ?>/img/logo-naruto.png" alt="Logo Naruto">
      </a>
        <nav class="menu-navegacao">
        <?php
                $args = array(
                    'menu' => 'principal',
                    'theme_location' => 'navegacao',
                    'container' => false
                );
                wp_nav_menu( $args ) 
                ?>
            <!-- <ul>             
                <li><a href="/produtos" classs="link-menu">Produtos</a></li>
                <li><a href="/noticias" classs="link-menu">Notícias</a></li>
                <li><a href="/sobre" classs="link-menu">Sobre</a></li>
                <li><a href="/contato" classs="link-menu">Contato</a></li>
            </ul> -->

        </nav>

    </header>
    <div class="header-division division"></div>