<?php 
    // Template Name: Produtos
?>

<?php get_header(); 

$news = new WP_Query(
    array(
        'posts_per_page' => 6,
        'post_type' => "post",
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'post_date',
        'order' => 'DESC',
    
        )
    );
?>

<h1>Produtos</h1>

<?php if($news -> have_posts(  )):
    while($news -> have_posts(  )):
        $news -> the_post(  ); ?>
        <div>
            <h2><?php the_title(  ); ?></h2>
            <a href="<?php the_permalink() ?>">Link para Produto!</a>
            <?php the_content( ); ?>
        </div>
    <?php endwhile;
else: ?>
    <p>Não temos produtos =(</p>
<?php endif; ?>

<?php get_footer(); ?>